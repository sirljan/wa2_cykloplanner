package wa2.servlets;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;
import wa2.Parameter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by Jenda on 17/05/15.
 */
@WebServlet(name = "FactorialServlet")
public class FactorialServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uri = request.getRequestURI();
        //throws IOException, TimeoutException, InterruptedException
        switch (uri) {
            case "/factorial":
                //add to queue
                System.out.println("starting rabbit mq");
                ConnectionFactory factory = new ConnectionFactory();
                factory.setHost("localhost");
                Connection connection = factory.newConnection();
                Channel channel = connection.createChannel();
                channel.queueDeclare(Parameter.FACTORIAL_QUEUE_NAME.toString(), false, false, false, null);
                String message = request.getQueryString();
                channel.basicPublish("", Parameter.FACTORIAL_QUEUE_NAME.toString(), null, message.getBytes());
                System.out.println(" [x] Sent '" + message + "'");

                String result = null;
                try {
                    result = receiveResult();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    e.printStackTrace();
                }
                response.getWriter().write(""+result);
                try {
                    channel.close();
                } catch (TimeoutException e) {
                    e.printStackTrace();
                }
                connection.close();
                break;
            default:
                break;
        }
    }

    private String receiveResult() throws IOException, InterruptedException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(Parameter.FACTORIAL_RESULT_QUEUE_NAME.toString(), false, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        QueueingConsumer consumer = new QueueingConsumer(channel);
        channel.basicConsume(Parameter.FACTORIAL_RESULT_QUEUE_NAME.toString(), true, consumer);
        boolean wait = true;
        String message = "";
        while (wait) {
            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
            message = new String(delivery.getBody());
            System.out.println(" [x] Received '" + message + "'");
            wait = false;
        }
        channel.close();
        connection.close();
        return message;
    }


}

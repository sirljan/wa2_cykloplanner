package wa2.servlets;

import org.hibernate.Query;
import org.hibernate.Session;
import wa2.HibernateUtil;
import wa2.models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Jenda on 13/05/15.
 */
@WebServlet(name = "UserServlet")
public class UserServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uri = request.getRequestURI();
        switch (uri) {
            case "/user/registerUser":
                 insertNewUser(request.getParameter("username"), request.getParameter("password"));
                response.getWriter().write("user is registered");
                break;
            case "/user/signIn":
                User user = getUser(request.getParameter("username"), request.getParameter("password"));
                if(user != null)
                    response.getWriter().write("you are signed in");
                else
                    response.getWriter().write("you entered wrong name or password");
                break;
            default:
                break;
        }
    }

    protected void insertNewUser(String name, String password){
        User user = new User();
        user.setUsername(name);
        user.setPassword(password);
        HibernateUtil.saveObject(user);
    }

    protected User getUser(String name, String password){
        final Session session = HibernateUtil.getSession();
        try {
            return getUserInSession(session,name,password);
        }
        finally {
            session.close();
        }
    }

    public User getUserInSession(Session session,String name, String password){
        String queryString = "SELECT u FROM wa2.models.User u where (u.username=:name AND u.password=:password)";
        final Query query = session.createQuery(queryString)
                .setParameter("name", name)
                .setParameter("password", password);
        System.out.println("executing: " + query.getQueryString());
        List<User> resultList = query.list();
        if (resultList.size()>0) {
            User user = resultList.get(0);
            return user;
        } else {
            return null;
        }
    }
}

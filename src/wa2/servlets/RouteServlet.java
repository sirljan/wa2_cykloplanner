package wa2.servlets;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import wa2.HibernateUtil;
import wa2.Parameter;
import wa2.models.Location;
import wa2.models.Route;
import wa2.models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

/**
 * Created by Jenda on 15/05/15.
 */
@WebServlet(name = "RouteServlet")
public class RouteServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uri = request.getRequestURI();
        boolean success;
        switch (uri) {
            case "/route/saveRoute":
                Location locationFrom;
                Location locationTo;
                String username = request.getParameter(Parameter.USERNAME.toString());
                String password = request.getParameter(Parameter.PASSWORD.toString());
                //locationIDs
                if (request.getParameter(Parameter.ROUTE_FROM.toString()).length()>0){
                    LocationServlet locationServlet = new LocationServlet();
                    locationFrom = locationServlet.getLocation(
                            username,
                            password,
                            Integer.parseInt(request.getParameter(Parameter.ROUTE_FROM.toString())));
                    locationTo = locationServlet.getLocation(
                            username,
                            password,
                            Integer.parseInt(request.getParameter(Parameter.ROUTE_TO.toString())));
                }
                //raw gps
                else {
                    locationFrom = new Location();
                    locationFrom.setLat(Double.parseDouble(request.getParameter(Parameter.ROUTE_FROM_LAT.toString())));
                    locationFrom.setLon(Double.parseDouble(request.getParameter(Parameter.ROUTE_FROM_LON.toString())));
                    locationTo = new Location();
                    locationTo.setLat(Double.parseDouble(request.getParameter(Parameter.ROUTE_FROM_LAT.toString())));
                    locationTo.setLon(Double.parseDouble(request.getParameter(Parameter.ROUTE_FROM_LON.toString())));
                }


                saveRoute(
                        username,
                        password,
                        request.getParameter(Parameter.ROUTE_NAME.toString()),
                        locationFrom,
                        locationTo
                );
                response.getWriter().write("route is saved");
                break;
            case "/route/getRoute":
                Route route = getRoute(request.getParameter(Parameter.USERNAME.toString()),
                        request.getParameter(Parameter.PASSWORD.toString()),
                        Integer.parseInt(request.getParameter(Parameter.ROUTE_ID.toString())));
                if (route != null) {
                    response.getWriter().write(route.toString());
                }
                else {
                    response.getWriter().write("route doesn't exist");
                }
                break;
            case "/route/deleteRoute":
                deleteRout(request.getParameter(Parameter.USERNAME.toString()),
                        request.getParameter(Parameter.PASSWORD.toString()),
                        Integer.parseInt(request.getParameter(Parameter.ROUTE_ID.toString())));
                response.getWriter().write("route was deleted");
                break;
            case "/route/calcDistance":
                route = getRoute(request.getParameter(Parameter.USERNAME.toString()),
                        request.getParameter(Parameter.PASSWORD.toString()),
                        Integer.parseInt(request.getParameter(Parameter.ROUTE_ID.toString())));
                if (route != null) {
                    if (route.getDistance() > 0) {
                        response.getWriter().write("Route distance is: " + route.getDistance());
                    } else {
                        //add to queue
                        System.out.println("starting rabbit mq");
                        ConnectionFactory factory = new ConnectionFactory();
                        factory.setHost("localhost");
                        Connection connection = factory.newConnection();
                        Channel channel = connection.createChannel();
                        channel.queueDeclare(Parameter.FACTORIAL_QUEUE_NAME.toString(), false, false, false, null);
                        String message = request.getQueryString()+"&method=calcDistance";
                        channel.basicPublish("", Parameter.FACTORIAL_QUEUE_NAME.toString(), null, message.getBytes());
                        System.out.println(" [x] Sent '" + message + "'");

                        response.getWriter().write("Request in progress...");
                        try {
                            channel.close();
                        } catch (TimeoutException e) {
                            e.printStackTrace();
                        }
                        connection.close();
                    }
                }
                break;
            default:
                break;
        }
    }

    private void saveRoute(String username, String password, String routeName, Location from, Location to) {
        UserServlet userServlet = new UserServlet();
        final Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try {
            User user = userServlet.getUserInSession(session, username, password);
            ArrayList<Location> locations = new ArrayList<>();
            locations.add(from);
            locations.add(to);
            Route route = new Route(user, locations, routeName);
            route.setUser(user);
            tx = session.beginTransaction();
            session.save(route);
            session.flush();
            tx.commit();
        } finally {
            session.close();
        }
    }

    public Route getRoute(String username, String password, int routeId){
        final Session session = HibernateUtil.getSession();
        try {
            //varianta JOIN FETCH (alternativa k FUTURE)
            String queryString =
                    "FROM wa2.models.Route route " +
//                    "JOIN FETCH route.locations "+
                    "WHERE (route.id=:routeId AND route.user.username=:name AND route.user.password=:password)";
            final Query query = session.createQuery(queryString)
                    .setParameter("name", username)
                    .setParameter("password", password)
                    .setParameter("routeId",routeId);
            System.out.println("executing: " + query.getQueryString());
            List<Route> resultList = query.list();  //nebo FUTURE varianta: query.future(); ...hibernate odlozi dotaz do db a ceka na šáhnutí na objekt.
            if (resultList.size()>0) {
                Route route = resultList.get(0);
                return route;
            } else {
                return null;
            }
        }
        finally {
            session.close();
        }
    }

    private void deleteRout(String username, String password, int routeId){

        Route route = getRoute(username, password, routeId);
        HibernateUtil.deleteObject(route);
    }

}

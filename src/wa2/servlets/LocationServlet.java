package wa2.servlets;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import wa2.Parameter;
import wa2.HibernateUtil;
import wa2.models.Location;
import wa2.models.Route;
import wa2.models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Jenda on 15/05/15.
 */
@WebServlet(name = "LocationServlet")
public class LocationServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uri = request.getRequestURI();
        switch (uri) {
            case "/location/insertNewLocation":
                double lon = Double.parseDouble(request.getParameter(Parameter.LOCATION_LONGITUTE.toString()));
                double lat = Double.parseDouble(request.getParameter(Parameter.LOCATION_LATITUDE.toString()));
                insertNewLocation(
                        request.getParameter(Parameter.USERNAME.toString()),
                        request.getParameter(Parameter.PASSWORD.toString()),
                        request.getParameter(Parameter.LOCATION_NAME.toString()),
                        lon,
                        lat
                        );
                response.getWriter().write("location inserted");
                break;
            case "/location/deleteLocation":
                int locId = Integer.parseInt(request.getParameter(Parameter.LOCATION_ID.toString()));
                deleteLocation(
                        request.getParameter(Parameter.USERNAME.toString()),
                        request.getParameter(Parameter.PASSWORD.toString()),
                        locId
                        );
                response.getWriter().write("location deleted");
                break;
            case "/location/getLocation":
                locId = Integer.parseInt(request.getParameter(Parameter.LOCATION_ID.toString()));
                Location location = getLocation(
                        request.getParameter(Parameter.USERNAME.toString()),
                        request.getParameter(Parameter.PASSWORD.toString()),
                        locId
                );
                response.getWriter().write(location.toString());
            default:
                break;
        }
    }
    private void insertNewLocation(String username, String password, String locationName, double lon, double lat) {
        UserServlet userServlet = new UserServlet();
        final Session session = HibernateUtil.getSession();
        Transaction tx = null;
        try {
            User user = userServlet.getUserInSession(session, username, password);

            Location location = new Location();
            location.setName(locationName);
            location.setLon(lon);
            location.setLat(lat);
            location.setUser(user);

            tx = session.beginTransaction();
            session.saveOrUpdate(location);
            session.flush();
            tx.commit();
        } finally {
            session.close();
        }
    }

    protected Location getLocation(String username, String password, int locId){
        final Session session = HibernateUtil.getSession();
        try {
            return getLocationInSession(session, username, password, locId);
        }
        finally {
            session.close();
        }
    }

    public Location getLocationInSession(Session session,String username, String password, int locId){
        String queryString = "SELECT loc " +
                "FROM wa2.models.Location loc " +
                "where (loc.id=:locId AND loc.user.username=:name AND loc.user.password=:password)";
        final Query query = session.createQuery(queryString)
                .setParameter("name", username)
                .setParameter("password", password)
                .setParameter("locId",locId);
        System.out.println("executing: " + query.getQueryString());
        List<Location> resultList = query.list();
        if (resultList.size()>0) {
            Location location = resultList.get(0);
            return location;
        } else {
            return null;
        }
    }

    private List<Route> getRouteInSessionWithLocation(Session session,String username, String password, int locId) {
        String queryString = "SELECT route " +
                "FROM wa2.models.Route route " +
                "where ((route.routeLocations.pk.locationId=:locId) AND (route.user.username=:name AND route.user.password=:password))";

//        queryString = "select route " +
//                "from wa2.models.RouteLocation routeLocation " +
//                "where routeLocation.location.locationId=:locId AND " +
//                "(routeLocation.route.user.username=:name AND routeLocation.route.user.password=:password)";
        final Query query = session.createQuery(queryString)
                .setParameter("name", username)
                .setParameter("password", password)
                .setParameter("locId",locId);
        List<Route> resultList = query.list();
        if (resultList.size()>0) {
            return resultList;
        } else {
            return null;
        }
    }

    private void deleteLocation(String username, String password, int locId){
        Location location = getLocation(username, password, locId);
        HibernateUtil.deleteObject(location);
    }
}

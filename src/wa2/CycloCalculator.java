package wa2;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;
import org.hibernate.Query;
import org.hibernate.Session;
import wa2.models.Route;
import wa2.models.RouteLocation;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

public class CycloCalculator {
    private final static String QUEUE_NAME = "factorial";
    private final static String RESULT_QUEUE_NAME = "factorialResult";
    public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        QueueingConsumer consumer = new QueueingConsumer(channel);
        channel.basicConsume(QUEUE_NAME, true, consumer);

        while (true) {
            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
            String message = new String(delivery.getBody());
            System.out.println(" [x] Received '" + message + "'");

            Map<String, String> myMap = new HashMap<String, String>();
            String query = message;
            String[] pairs = query.split("&");
            for (int i=0;i<pairs.length;i++) {
                String pair = pairs[i];
                String[] keyValue = pair.split("=");
                myMap.put(keyValue[0], keyValue[1]);
            }

            if (myMap.containsKey("method"))
            {
                String messageMethod = myMap.get("method");
                if (messageMethod.equals("calcDistance")){
                    String username = myMap.get("username");
                    String password = myMap.get("password");
                    String routeIdString = myMap.get("routeId");
                    int routeId = Integer.parseInt(routeIdString);
                    List<RouteLocation> routeLocations = getRouteLocationList(username, password, routeId);

                    double distance = 0;
                    for (int i = 0; i < routeLocations.size()-1; i++) {
                        double x1 = routeLocations.get(i).getLocation().getLon();
                        double y1 = routeLocations.get(i).getLocation().getLat();
                        double x2 = routeLocations.get(i + 1).getLocation().getLon();
                        double y2 = routeLocations.get(i + 1).getLocation().getLat();
                        distance += Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
                    }
                    System.out.println("distance: "+distance);
                    Route route = getRoute(username, password, routeId);

                    route.setDistance(distance);
                    HibernateUtil.saveObject(route);
                }
                else if (messageMethod == "factorial"){
                    if (myMap.containsKey("factNum")) {
                        String factNum = myMap.get("factNum");
                        int n = Integer.parseInt(factNum);
                        long fact = factorial(n);
                        returnFactorialResult(fact);
                    }
                }
            }
        }
    }

    private static Route getRoute(String username, String password, int routeId) {
        final Session session = HibernateUtil.getSession();
        try {
            String queryString =
                    "FROM wa2.models.Route route " +
                            "WHERE (route.id=:routeId " +
                            "AND route.user.username=:name AND route.user.password=:password)";

            final Query query = session.createQuery(queryString)
                    .setParameter("name", username)
                    .setParameter("password", password)
                    .setParameter("routeId",routeId);
            System.out.println("executing: " + query.getQueryString());
            List<Route> resultList = query.list();  //nebo FUTURE varianta: query.future(); ...hibernate odlozi dotaz do db a ceka na šáhnutí na objekt.
            if (resultList.size() > 0) {
                return resultList.get(0);
            } else {
                return null;
            }
        }
        finally {
            session.close();
        }
    }

    private static void returnFactorialResult(long fact) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare(RESULT_QUEUE_NAME, false, false, false, null);
        String message = ""+fact;
        channel.basicPublish("", RESULT_QUEUE_NAME, null, message.getBytes());
        System.out.println(" [x] Sent '" + message + "'");
        channel.close();
        connection.close();
    }

    public static List<RouteLocation> getRouteLocationList(String username, String password, int routeId){
        final Session session = HibernateUtil.getSession();
        try {
//            String queryString =
//                    "FROM wa2.models.Location location " +
//                            "JOIN FETCH location.routeLocations JOIN FETCH location.routeLocations.route "+
//                            "WHERE (location.routeLocations.route.id=:routeId " +
//                            "AND location.routeLocations.route.user.username=:name AND location.routeLocations.route.user.password=:password) " +
//                            "order by location.routeLocations.orderNumber";
            String queryString =
                    "FROM wa2.models.RouteLocation routeLocation " +
//                    "JOIN FETCH routeLocation.location JOIN FETCH routeLocation.route " +
                    "WHERE (routeLocation.route.id=:routeId) " +
//                    "AND routeLocation.route.user.username=:name AND routeLocation.route.user.password=:password) " +
                    "order by routeLocation.orderNumber";
            final Query query = session.createQuery(queryString)
                    .setParameter("name", username)
                    .setParameter("password", password)
                    .setParameter("routeId",routeId);
            System.out.println("executing: " + query.getQueryString());
            List<RouteLocation> resultList = query.list();  //nebo FUTURE varianta: query.future(); ...hibernate odlozi dotaz do db a ceka na šáhnutí na objekt.
            System.out.println("konec worker");
            return resultList;
        }
        finally {
            session.close();
        }

    }

    private static long factorial(int n) {
        long fact = 1; // this  will be the result
        for (int i = 1; i <= n; i++) {
            fact *= i;
        }
        return fact;
    }
}

package wa2;

public enum Parameter {
    USERNAME("username"),
    PASSWORD("password"),

    LOCATION_NAME("locName"),
    LOCATION_LONGITUTE("locLon"),
    LOCATION_LATITUDE("locLan"),
    LOCATION_ID("locId"),

    ROUTE_ID("routeId"),
    ROUTE_NAME("routeName"),
    ROUTE_FROM_LON("routeFromLon"),
    ROUTE_FROM_LAT("routeFromLat"),
    ROUTE_TO_LON("routeToLon"),
    ROUTE_TO_LAT("routeToLat"),
    ROUTE_FROM("routeFrom"),
    ROUTE_TO("routeTo"),

    FACTORIAL_NUM("factNum"),

    FACTORIAL_QUEUE_NAME("factorial"),
    FACTORIAL_RESULT_QUEUE_NAME("factorialResult")
    ;

    private final String text;

    /**
     * @param text
     */
    private Parameter(final String text) {
        this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}

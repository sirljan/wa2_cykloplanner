package wa2;

import wa2.models.Location;
import wa2.models.Route;
import wa2.models.User;

import java.util.ArrayList;

/**
 * Created by Jenda on 13/05/15.
 */
public class Main {

    public static void main(final String[] args) throws Exception {
        User user = new User();
        user.setUsername("defaultUsername");
        user.setPassword("somePassword");
        HibernateUtil.saveObject(user);

        Location location = new Location();
        location.setUser(user);
        location.setLat(11.11);
        location.setLon(22.22);
        location.setName("defaultLocationName");
        HibernateUtil.saveObject(location);

        Location location2 = new Location();
        location2.setUser(user);
        location2.setLat(33.11);
        location2.setLon(44.22);
        location2.setName("defaultLocationName2");
        HibernateUtil.saveObject(location2);

        Location location3 = new Location();
        location3.setUser(user);
        location3.setLat(55.11);
        location3.setLon(44.22);
        location3.setName("defaultLocationName3");
        HibernateUtil.saveObject(location3);

        ArrayList<Location> locations = new ArrayList<>();
        locations.add(location);
        locations.add(location2);
        locations.add(location3);

        Route route = new Route(user, locations, "defaultRouteName");
        HibernateUtil.saveObject(route);

        HibernateUtil.queryAllEntities();
    }
}

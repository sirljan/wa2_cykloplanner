package wa2.models;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Jenda on 13/05/15.
 */
@Entity
@Table(name = "location")
public class Location {
    private int id;
    private User user;
    private Set<RouteLocation> routeLocations = new HashSet<>();
    private String name;
    private double lon;
    private double lat;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "location_id", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    @Cascade(CascadeType.DETACH)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        this.user.getLocations().add(this);
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
    public Set<RouteLocation> getRouteLocations() {
        return routeLocations;
    }

    public void setRouteLocations(Set<RouteLocation> routeLocations) {
        this.routeLocations = routeLocations;
    }

    @Override
    public String toString() {
        String string = "Location: " + this.getName() +
                "\nlon: "+this.getLon() +
                "\nlat: "+this.getLat();
        return string;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Location location = (Location) o;

        if (id != location.id) return false;
        if (Double.compare(location.lon, lon) != 0) return false;
        if (Double.compare(location.lat, lat) != 0) return false;
        if (user != null ? !user.equals(location.user) : location.user != null) return false;
        if (routeLocations != null ? !routeLocations.equals(location.routeLocations) : location.routeLocations != null)
            return false;
        return !(name != null ? !name.equals(location.name) : location.name != null);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (routeLocations != null ? routeLocations.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        temp = Double.doubleToLongBits(lon);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(lat);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}

package wa2.models;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Jenda on 13/05/15.
 */
@Entity
@Table(name = "route")
public class Route {
    private int id;
    private User user;
    private Set<RouteLocation> routeLocations = new HashSet<>();
    private String name;
    private double distance;

    public Route(){}

    public Route(User user, Location from, Location to, String name) {
        this.user = user;

        RouteLocation routeLocation = new RouteLocation();
        routeLocation.setRoute(this);
        routeLocation.setLocation(from);
        routeLocation.setOrderNumber(0);
        this.routeLocations.add(routeLocation);

        routeLocation = new RouteLocation();
        routeLocation.setRoute(this);
        routeLocation.setLocation(from);
        routeLocation.setOrderNumber(1);
        this.routeLocations.add(routeLocation);

        this.name = name;
    }

    public Route(User user, List<Location> locations, String name) {
        this.user = user;
        RouteLocation routeLocation;
        for (Location location : locations) {
            routeLocation = new RouteLocation();
            routeLocation.setRoute(this);
            routeLocation.setLocation(location);
            routeLocation.setOrderNumber(locations.indexOf(location));
            this.routeLocations.add(routeLocation);
        }
        this.name = name;
    }

    public Route(User user, Set<RouteLocation>routeLocations, String name) {
        this.user = user;
        this.routeLocations = routeLocations;
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "route_id", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    @Cascade(CascadeType.DETACH)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        this.user.getRoutes().add(this);
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "route", cascade= javax.persistence.CascadeType.ALL)
    public Set<RouteLocation> getRouteLocations() {
        return routeLocations;
    }

    public void setRouteLocations(Set<RouteLocation> routeLocations) {
        this.routeLocations = routeLocations;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        String string = "Route: "+name+"\nlocations:";
        return string;
    }
}

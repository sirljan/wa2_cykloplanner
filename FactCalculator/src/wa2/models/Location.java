package wa2.models;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Jenda on 13/05/15.
 */
@Entity
@Table(name = "location")
public class Location {
    private int id;
    private User user;
    private Set<Route> routeSet = new HashSet<Route>();
//    private Set<Route> routeSetFrom = new HashSet<>();
//    private Set<Route> routeSetTo = new HashSet<>();
    private String name;
    private double lon;
    private double lat;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "location_id", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    @Cascade(CascadeType.DETACH)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        this.user.getLocations().add(this);
    }

    @Override
    public String toString() {
        String string = "Location: " + this.getName() +
                "\nlon: "+this.getLon() +
                "\nlat: "+this.getLat();
        return string;
    }

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "locationSet")
    public Set<Route> getRouteSet() {
        return routeSet;
    }

    public void setRouteSet(Set<Route> routeSet) {
        this.routeSet = routeSet;
    }
}

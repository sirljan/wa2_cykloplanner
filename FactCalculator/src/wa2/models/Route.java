package wa2.models;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import java.util.ArrayList;

/**
 * Created by Jenda on 13/05/15.
 */
@Entity
@Table(name = "route")
public class Route {
    private int id;
    private User user;
    private ArrayList<Location> locations = new ArrayList<Location>();
    private String name;
    private double distance;

    public Route(){}

    public Route(User user, Location from, Location to, String name) {
        this.user = user;
        ArrayList<Location> locations = new ArrayList<>();
        locations.add(0,from);
        locations.add(to);
        this.name = name;
    }

    public Route(User user, ArrayList<Location>locations, String name) {
        this.user = user;
        this.locations = locations;
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "route_id", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    @Cascade(CascadeType.DETACH)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        this.user.getRoutes().add(this);
    }

    @ManyToMany(fetch = FetchType.LAZY, cascade = javax.persistence.CascadeType.ALL)
    @JoinTable(name = "route_location", joinColumns = {
            @JoinColumn(name = "route_id", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "location_id",
                    nullable = false, updatable = false) })
    public ArrayList<Location> getLocations() {
        return locations;
    }

    public void setLocations(ArrayList<Location> locationSet) {
        this.locations = locationSet;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        String string = "Route: "+name+"\nlocations:"+locations.toString();
        return string;
    }
}
